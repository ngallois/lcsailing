<?php

/**
 * Includes 'style.css'.
 * Disable this filter if you don't use child style.css file.
 *
 * @param  assoc $default_set set of styles that will be loaded to the page
 * @return assoc
 */
function filter_adventure_tours_get_theme_styles( $default_set ) {
	$default_set['child-style'] = get_stylesheet_uri();
	return $default_set;
}
add_filter( 'get-theme-styles', 'filter_adventure_tours_get_theme_styles' );


add_filter( 'getperson', 'getperson' );
function getperson() {

	$title = '';
	$byPerson = get_post_meta( get_the_ID(), 'person', true );
	if ( $byPerson ) {
		$title = '/pers';
	}
	return $title;

}

// Let's wait for the button to be clicked on
add_action( 'template_redirect', 'empty_cart_button_handler' );
function empty_cart_button_handler() {
	global $woocommerce;
	$items = $woocommerce->cart->get_cart();


    if( sizeof($items) ) {
			foreach ($items as $item => $value ) {
					create_new_order($value['product_id'],$value['quantity'],$value['date']);
			}
      WC()->cart->empty_cart( true );
    }
}


function create_new_order($product_id, $quantity , $date) {

  global $woocommerce;

	$customer = WC()->session->get( 'tour_order_data' );

  $address = array(
      'first_name' => $customer['billing_first_name'],
      'last_name'  => $customer['billing_last_name'],
      'company'    => '00000',
      'email'      => $customer['billing_email'],
      'phone'      => $customer['billing_phone'],
      'address_1'  => '00000 00000',
      'address_2'  => '0000',
      'city'       => 'Paris',
      'state'      => 'Pa',
      'postcode'   => '00000',
      'country'    => 'FR'
  );

  // Now we create the order
  $order = wc_create_order();

  // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
  $order->add_product( get_product($product_id), $quantity); // This is an existing SIMPLE product
	$order_item = $order->get_items();
	foreach( $order_item as $item_id => $product ) {
				//get values for vars
				$addMeta = $customer['date'];
                                //add new meta
				wc_add_order_item_meta($item_id, 'tour_date', $addMeta, false);
			}
  $order->set_address( $address, 'billing' );
	$order->set_address( $address, 'shipping' );
  //
  $order->calculate_totals();
	$order->add_order_note('Date: '. $customer['date']);
  //$order->update_status("Completed", 'Imported order', TRUE);
	// Store Order ID in session so it can be re-used after payment failure
    WC()->session->order_awaiting_payment = $order->id;

    // Process Payment
    //$available_gateways = WC()->payment_gateways->get_available_payment_gateways();
    //$result = $available_gateways[ 'cod' ]->process_payment( $order->id );


    // Redirect to success/confirmation/payment page
    if ( $result['result'] == 'success' ) {

        $result = apply_filters( 'woocommerce_payment_successful_result', $result, $order->id );

        wp_redirect( $result['redirect'] );
        exit;
    }else{

        //example URL
        // /checkout/order-received/1385/?key=wc_order_5b43e00081a8b
        $order->update_status( 'on-hold' );
        $woocommerce->cart->empty_cart();
        wp_redirect($order->get_checkout_order_received_url());
        exit;
    }

}

add_shortcode('modal-booknow','modalBookNow');


function modalBookNow(){
    global $product;
    $booking_form_location = adventure_tours_get_booking_form_location_for_tour( $product );
    $booking_dates = $booking_form_location ? adventure_touts_get_tour_booking_dates( $product->get_id() ) : null;


    $html = '<!-- Botón de apertura -->
	<p><button type="button" class="btn btn-info btn-lg click-modal book-now hidden-xs " data-toggle="modal" data-target="#myModal">
	On en parle ?
	</button>
	<a href="#tourForm" class="btn btn-info btn-lg visible-xs book-now" >
	On en parle ?
	</a>
	</p>

	<script>
	jQuery(document).ready(function( $ ) {

		$(".click-modal").on("click", function() {
			$(".form-block--tour-booking").prependTo("#modal2");
		});

		$(".close").on("click", function(){
				$(".form-block--tour-booking").appendTo("aside");
				$(".form-block--tour-booking").appendTo(".booking-form-wrapper");
		});

		$("#myModal").on("click",function(e) {

        var container = $(".modal-dialog");

            if (!container.is(e.target) && container.has(e.target).length === 0) {
            //Se ha pulsado en cualquier lado fuera de los elementos contenidos en la variable container
									$(".form-block--tour-booking").appendTo("aside");
									$(".form-block--tour-booking").appendTo(".booking-form-wrapper");
            }
   });

	});
	</script>
';

    if( $booking_dates ){
        return $html;
    }

}

function modalHtml(){
	$html = '<!-- Ventana Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body" id="modal2">

			</div>

		</div>
	</div>
	</div>';
	echo $html;
}
