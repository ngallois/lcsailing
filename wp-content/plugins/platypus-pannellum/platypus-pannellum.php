<?php
/**
 * Plugin Name: Pannellum (image in 360º)
 * Plugin URI: http://www.platypus-agency.fr/
 * Description: This plugin places by means of a shortcode an iframe with a 360º image
 * Version: 1.0.0
 * Author: Gennys Alexander Carrasquilla Estremor
 * Author URI: http://www.gnnslxndr.io
 * Requires at least:
 * Tested up to:
 *
 * Text Domain: platypus-pannellum
 * Domain Path: /languages/
 */
defined( 'ABSPATH' ) or die( '¡Sin trampas!' );

if ( !is_admin() ) {
    require_once( 'public/iframe-pannellum.php' );
} 
