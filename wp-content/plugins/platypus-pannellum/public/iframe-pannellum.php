<?php
/**
 * This file contains the necessary functions to modify the front-end.
 *
 *
 * @author  Gennys Alexander Carrasquilla <
 *
 * @link
 * @since   1.0.0
 */



//Function that renders the div with the image in 360º

 function pannellum_shortcode( $atts ){

     //Load the libraries js and styles css of pannelum version 2.4
   wp_enqueue_script( 'pannellumjs', plugin_dir_url(__FILE__).'js/2.4/pannellum.js', array(), null, false );
    wp_enqueue_style( 'pannellumcss', plugin_dir_url(__FILE__).'css/2.4/pannellum.css', array(), null, false );

   ob_start();
     //Record the parameters to use them in the function
    $pannellum = shortcode_atts(array(
              	'autoload' => 'true',
              	'image' => '',
                'autorotate' => '10',
                'preview' => '',
                'width' => '600px',
                'height' => '400px',
                'type' => 'equirectangular'
              ), $atts);

   include( 'partials/iframe.php' );
   $outputs = ob_get_clean();

   return $outputs;
 }

 add_shortcode('pannellum', 'pannellum_shortcode');

 function pannellum_video_shortcode( $atts ){

     //Load the libraries js and styles css of pannelum version 2.4
     wp_enqueue_script( 'pannellumjs', plugin_dir_url(__FILE__).'js/2.4/pannellum.js', array(), null, false );
     wp_enqueue_style( 'pannellumcss', plugin_dir_url(__FILE__).'css/2.4/pannellum.css', array(), null, false );

     wp_enqueue_script( 'video', plugin_dir_url(__FILE__).'js/video/video.js', array(), null, false );
     wp_enqueue_script( 'videojs-pannellum-plugin', plugin_dir_url(__FILE__).'js/video/videojs-pannellum-plugin.js', array(), null, false );
     wp_enqueue_style( 'video-js', plugin_dir_url(__FILE__).'css/video/video-js.css', array(), null, false );

     ob_start();
     //Record the parameters to use them in the function
     $pannellum = shortcode_atts(array(
         'mp4' => '',
         'poster' => '',
         'width' => '600px',
         'height' => '400px'
     ), $atts);

     include( 'partials/video.php' );
     $outputs = ob_get_clean();

     return $outputs;
 }

 add_shortcode("pannellum-video", "pannellum_video_shortcode");

?>
