<style>
#panorama {
    width: <?php echo $pannellum['width']; ?>;
    height: <?php echo $pannellum['height']; ?>;
}
</style>
<!-- Div to render -->
<div id="panorama"></div>
<script type="text/javascript">

jQuery(function($){
  pannellum.viewer('panorama', {
      "type": "<?php echo $pannellum['type']; ?>",
      "autoLoad": <?php echo $pannellum['autoload']; ?>,
      "autoRotate": "<?php echo $pannellum['autorotate']; ?>",
      "preview" : "<?php echo $pannellum['preview']; ?>",
      "panorama": "<?php echo $pannellum['image']; ?>"
  });
});

</script>
