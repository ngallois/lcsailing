<?php
/**
 * User: gennysalexander
 * Date: 26/04/18
 * Time: 1:34 PM
 */
?>
<style>
    #panorama-video {
        width: <?php echo $pannellum['width']; ?>;
        height: <?php echo $pannellum['height']; ?>;
    }
</style>
<video id="panorama-video" class="video-js vjs-default-skin vjs-big-play-centered"
       controls preload="none"
       poster="<?php echo $pannellum['poster']; ?>" crossorigin="anonymous">
    <!--<source src="/images/video/jfk.webm" type="video/webm"/> -->
    <source src="<?php echo $pannellum['mp4']; ?>" type="video/mp4"/>
    <p class="vjs-no-js">
        To view this video please enable JavaScript, and consider upgrading to
        a web browser that <a href="http://videojs.com/html5-video-support/"
                              target="_blank">supports HTML5 video</a>
    </p>
</video>

<script>
    jQuery(function($) {
        videojs('panorama-video', {
            plugins: {
                pannellum: {
                    "autoLoad" : true,
                    "autoRotate" : 10
                }
            }
        });
    });
</script>
