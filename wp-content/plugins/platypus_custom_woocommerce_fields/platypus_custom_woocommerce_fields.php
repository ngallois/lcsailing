<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the
 * plugin admin area. This file also includes all of the dependencies used by
 * the plugin, and defines a function that starts the plugin.
 *
 *
 * @wordpress-plugin
 * Plugin Name:       platypus Custom WooCommerce Fields
 * Description:       Create new field (person) for show if a tour is global or personal.
 * Version:           1.0.0
 * Author:            Gennys Alexander Carrasquilla
 * Author URI:        gnnslxndr
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

defined( 'WPINC' ) || die;

include_once 'admin/class-platypus-custom-woocommerce-field.php';
include_once 'public/class-platypus-custom-woocommerce-display.php';

add_action( 'plugins_loaded', 'byperson_wc_input_start' );
/**
 * Start the plugin.
 */
function byperson_wc_input_start() {

	if ( is_admin() ) {

		$admin = new Platypus_Custom_WooCommerce_Field( 'person' );
		$admin->init();

	} else {

		$plugin = new Platypus_Custom_WooCommerce_Display( 'person' );
		$plugin->init();
	}
}
