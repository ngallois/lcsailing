<?php
/**
 * Demonstrates how to add a custom field to the 'Linked Products' tab of a
 * Simple Product in WooCommerce.
 *
 * @package CWF\Admin
 */

/**
 * Demonstrates how to add a custom field to the 'Linked Products' tab of a
 * Simple Product in WooCommerce.
 *
 * This plugin is a demo for a corresponding Tuts+ tutorial.
 *
 * @package CWF\Admin
 */
class Platypus_Custom_WooCommerce_Field {

	/**
	 * Maintains a value to the text field ID for serialization.
	 *
	 * @access private
	 * @var    string
	 */
	private $textfield_id;

	/**
	 * Initializes the class and the instance variables.
	 */
	public function __construct( $id ) {
		$this->textfield_id = $id;
	}

	/**
	 * Initializes the hooks for adding the text field and saving the values.
	 */
	public function init() {

		add_action(
			'woocommerce_product_options_general_product_data',
			array( $this, 'product_options_general_product_data' )
		);

		add_action(
			'woocommerce_process_product_meta',
			array( $this, 'add_custom_linked_field_save' )
		);
	}

	/**
	 * Initializes and renders the contents of the text field.
	 */
	public function product_options_general_product_data() {

		$args =  array(
      	'id'            => $this->textfield_id,
       	'label'         => sanitize_text_field( 'By Person' ),
      	'description'   => 'Check when the prices is by person',
        'desc_tip'      => true,
        'value'         => get_post_meta( get_the_ID(), $this->textfield_id, true ),
        'cbvalue'       => '1'
      );
    // Checkbox
woocommerce_wp_checkbox( $args );
	}

	/**
	 * Saves a sanitized version of the VHX Package ID values provided by the user.
	 *
	 * @param int $post_id The ID of the current post to which the IDs are associated.
	 */
	public function add_custom_linked_field_save( $post_id ) {

		if ( ! ( isset( $_POST['woocommerce_meta_nonce'], $_POST[ $this->textfield_id ] ) || wp_verify_nonce( sanitize_key( $_POST['woocommerce_meta_nonce'] ), 'woocommerce_save_data' ) ) ) { // Input var okay.
			return false;
		}

		$byPerson = sanitize_text_field(
			wp_unslash( $_POST[ $this->textfield_id ] ) // Input var okay.
		);

		update_post_meta(
			$post_id,
			$this->textfield_id,
			esc_attr( $byPerson )
		);
	}
}
