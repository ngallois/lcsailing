<?php
/**
 *
 * @package CWF\Public
 */
class Platypus_Custom_WooCommerce_Display {

	/**
	 * Maintains a value to the text field ID for serialization.
	 *
	 * @access private
	 * @var    string
	 */
	private $textfield_id;

	/**
	 * Initializes the class and the instance variables.
	 */
	public function __construct( $id ) {
		$this->textfield_id = $id;
	}

	/**
	 * Registers hooks for displaying data on the front-end of the site.
	 */
	public function init() {

		add_filter( 'get_person', 'getperson');
	}

	/**
	 * Retrieves the metadata and echos it just below the product thumbnail.
	 */
	public function getperson($arg = '' ) {

		$title = '';
		$byPerson = get_post_meta( get_the_ID(), $this->textfield_id, true );
		if ( $byPerson ) {
			$title = '/pers';
		}
		return $title;

	}
}
